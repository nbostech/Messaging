FROM java:8
ADD target/messaging.jar messaging.jar
ENTRYPOINT ["java","-jar","messaging.jar"]
