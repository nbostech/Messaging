These are the usecases for MessagingAndAlerts:


Messages:
1. Load the last conversations.
2. Load all messages for a particular conversation.
3. Delete the conversation.

Alerts:
1. Send an Alert.
2. Check the sentAlerts.
3. Check the receivedAlerts.
4. Load the Alert.