package com.wavelabs.resource.test;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.http.ResponseEntity;

import com.wavelabs.entity.User;
import com.wavelabs.entity.UserTokenInfo;
import com.wavelabs.model.test.ObjectBuilder;
import com.wavelabs.resource.UserController;
import com.wavelabs.services.UserService;
import com.wavelabs.services.UserTypeChecker;

@RunWith(PowerMockRunner.class)
@PrepareForTest(UserTypeChecker.class)
public class UserControllerTest {
	@Mock
	private UserService userService;
	@InjectMocks
	private UserController userResource;

	@Test
	public void testPersistUser() {
		PowerMockito.mockStatic(UserTypeChecker.class);
		User user = ObjectBuilder.getUser();
		UserTokenInfo userTokenInfo = ObjectBuilder.getUserTokenInfo();
		when(UserTypeChecker.isGuestUser(userTokenInfo)).thenReturn(true);
		when(userService.saveUser(user)).thenReturn(true);
		@SuppressWarnings("rawtypes")
		ResponseEntity entity = userResource.createUser(user, userTokenInfo);
		Assert.assertEquals(200, entity.getStatusCodeValue());

	}

	@Test
	public void testPersistUser1() {
		PowerMockito.mockStatic(UserTypeChecker.class);
		User user = ObjectBuilder.getUser();
		UserTokenInfo userTokenInfo = ObjectBuilder.getUserTokenInfo();
		when(UserTypeChecker.isGuestUser(userTokenInfo)).thenReturn(true);
		when(userService.saveUser(user)).thenReturn(false);
		@SuppressWarnings("rawtypes")
		ResponseEntity entity = userResource.createUser(user, userTokenInfo);
		Assert.assertEquals(500, entity.getStatusCodeValue());

	}

	@Test
	public void testPersistUser2() {
		PowerMockito.mockStatic(UserTypeChecker.class);
		User user = ObjectBuilder.getUser();
		UserTokenInfo userTokenInfo = ObjectBuilder.getUserTokenInfo();
		when(UserTypeChecker.isGuestUser(userTokenInfo)).thenReturn(false);
		when(userService.saveUser(user)).thenReturn(false);
		@SuppressWarnings("rawtypes")
		ResponseEntity entity = userResource.createUser(user, userTokenInfo);
		Assert.assertEquals(403, entity.getStatusCodeValue());

	}

	@Test
	public void testGetUser() {
		PowerMockito.mockStatic(UserTypeChecker.class);
		User user = ObjectBuilder.getUser();
		when(userService.getUser(anyLong())).thenReturn(user);
		@SuppressWarnings("rawtypes")
		ResponseEntity entity = userResource.getUser(3);
		Assert.assertEquals(200, entity.getStatusCodeValue());
	}

	@Test
	public void testgetUser1() {
		PowerMockito.mockStatic(UserTypeChecker.class);
		@SuppressWarnings("unused")
		User user = ObjectBuilder.getUser();
		when(userService.getUser(anyLong())).thenReturn(null);
		@SuppressWarnings("rawtypes")
		ResponseEntity entity = userResource.getUser(3);
		Assert.assertEquals(500, entity.getStatusCodeValue());
	}

	/*@Test
	public void testDeleteUser() {
		PowerMockito.mockStatic(UserTypeChecker.class);
		UserTokenInfo userTokenInfo = ObjectBuilder.getUserTokenInfo();
		@SuppressWarnings("unused")
		User user = ObjectBuilder.getUser();
		when(UserTypeChecker.isGuestUser(any(UserTokenInfo.class))).thenReturn(false);
		when(userService.deleteUser(anyInt())).thenReturn(true);
		@SuppressWarnings("rawtypes")
		ResponseEntity entity = userResource.deleteUser(3, userTokenInfo);
		Assert.assertEquals(200, entity.getStatusCodeValue());
	}

	@Test
	public void testDeleteUser1() {
		PowerMockito.mockStatic(UserTypeChecker.class);
		UserTokenInfo userTokenInfo = ObjectBuilder.getUserTokenInfo();
		@SuppressWarnings("unused")
		User user = ObjectBuilder.getUser();
		when(UserTypeChecker.isGuestUser(any(UserTokenInfo.class))).thenReturn(false);
		when(userService.deleteUser(anyInt())).thenReturn(false);
		@SuppressWarnings("rawtypes")
		ResponseEntity entity = userResource.deleteUser(3, userTokenInfo);
		Assert.assertEquals(500, entity.getStatusCodeValue());
	}

	@Test
	public void testDeleteUser2() {
		PowerMockito.mockStatic(UserTypeChecker.class);
		UserTokenInfo userTokenInfo = ObjectBuilder.getUserTokenInfo();
		@SuppressWarnings("unused")
		User user = ObjectBuilder.getUser();
		when(UserTypeChecker.isGuestUser(any(UserTokenInfo.class))).thenReturn(true);
		when(userService.deleteUser(anyInt())).thenReturn(false);
		@SuppressWarnings("rawtypes")
		ResponseEntity entity = userResource.deleteUser(3, userTokenInfo);
		Assert.assertEquals(403, entity.getStatusCodeValue());
	}
*/
}
