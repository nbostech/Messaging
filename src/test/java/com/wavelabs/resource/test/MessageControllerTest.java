package com.wavelabs.resource.test;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.http.ResponseEntity;

import com.wavelabs.entity.Message;
import com.wavelabs.entity.UserTokenInfo;
import com.wavelabs.model.test.ObjectBuilder;
import com.wavelabs.resource.MessageController;
import com.wavelabs.services.MessageService;
import com.wavelabs.services.UserTypeChecker;

@RunWith(PowerMockRunner.class)
@PrepareForTest(UserTypeChecker.class)
public class MessageControllerTest {
	@Mock
	MessageService messageService;
	@InjectMocks
	MessageController messageResource;

	@Test
	public void testPersistMessage() {
		PowerMockito.mockStatic(UserTypeChecker.class);
		Message message = ObjectBuilder.getMessage();
		UserTokenInfo userTokenInfo = ObjectBuilder.getUserTokenInfo();
		when(messageService.saveMessage(any())).thenReturn(true);
		when(UserTypeChecker.isGuestUser(any())).thenReturn(false);
		@SuppressWarnings("rawtypes")
		ResponseEntity entity = messageResource.createMessage(message, userTokenInfo);
		Assert.assertEquals(200, entity.getStatusCodeValue());

	}

	@Test
	public void testPersistMessage1() {
		PowerMockito.mockStatic(UserTypeChecker.class);
		Message message = ObjectBuilder.getMessage();
		UserTokenInfo userTokenInfo = ObjectBuilder.getUserTokenInfo();
		when(messageService.saveMessage(any(Message.class))).thenReturn(false);
		when(UserTypeChecker.isGuestUser(any())).thenReturn(false);
		@SuppressWarnings("rawtypes")
		ResponseEntity entity = messageResource.createMessage(message, userTokenInfo);
		Assert.assertEquals(500, entity.getStatusCodeValue());

	}

	@Test
	public void testPersistMessage3() {
		PowerMockito.mockStatic(UserTypeChecker.class);
		Message message = ObjectBuilder.getMessage();
		UserTokenInfo userTokenInfo = ObjectBuilder.getUserTokenInfo();
		when(messageService.saveMessage(any())).thenReturn(false);
		when(UserTypeChecker.isGuestUser(any())).thenReturn(true);
		@SuppressWarnings("rawtypes")
		ResponseEntity entity = messageResource.createMessage(message, userTokenInfo);
		Assert.assertEquals(403, entity.getStatusCodeValue());

	}

	@Test
	public void testgetConversations() {
		PowerMockito.mockStatic(UserTypeChecker.class);
		Message[] message = { new Message() };
		UserTokenInfo userTokenInfo = ObjectBuilder.getUserTokenInfo();
		when(messageService.getLatestMessageFromAllConversations(anyInt())).thenReturn(message);
		when(UserTypeChecker.isGuestUser(any())).thenReturn(false);
		@SuppressWarnings("rawtypes")
		ResponseEntity entity = messageResource.getConversations(3, userTokenInfo);
		Assert.assertEquals(200, entity.getStatusCodeValue());
	}

	@Test
	public void testgetConversations1() {
		PowerMockito.mockStatic(UserTypeChecker.class);
		Message[] message = { new Message(), new Message() };
		UserTokenInfo userTokenInfo = ObjectBuilder.getUserTokenInfo();
		when(messageService.getLatestMessageFromAllConversations(anyInt())).thenReturn(message);
		when(UserTypeChecker.isGuestUser(any())).thenReturn(true);
		@SuppressWarnings("rawtypes")
		ResponseEntity entity = messageResource.getConversations(4, userTokenInfo);
		Assert.assertEquals(403, entity.getStatusCodeValue());
	}

	@Test
	public void testgetConversations2() {
		PowerMockito.mockStatic(UserTypeChecker.class);
		Message[] message = { new Message(), new Message() };
		UserTokenInfo userTokenInfo = ObjectBuilder.getUserTokenInfo();
		when(messageService.getLatestMessageFromAllConversations(anyInt())).thenReturn(message);
		when(UserTypeChecker.isGuestUser(any())).thenReturn(true);
		@SuppressWarnings("rawtypes")
		ResponseEntity entity = messageResource.getConversations(2, userTokenInfo);
		Assert.assertEquals(403, entity.getStatusCodeValue());
	}

	@SuppressWarnings("rawtypes")
	@Test
	public void testgetConversationMessages() {
		PowerMockito.mockStatic(UserTypeChecker.class);
		Message[] msg = { new Message(), new Message() };
		UserTokenInfo userTokenInfo = ObjectBuilder.getUserTokenInfo();
		when(messageService.getConversationMessages(anyInt())).thenReturn(msg);
		when(UserTypeChecker.isGuestUser(userTokenInfo)).thenReturn(false);
		ResponseEntity entity = messageResource.getConversationMessages(anyInt(), userTokenInfo);
		Assert.assertEquals(200, entity.getStatusCodeValue());

	}

	@SuppressWarnings("rawtypes")
	@Test
	public void testgetConversationMessages1() {
		PowerMockito.mockStatic(UserTypeChecker.class);
		UserTokenInfo userTokenInfo = ObjectBuilder.getUserTokenInfo();
		when(messageService.getConversationMessages(anyInt())).thenReturn(null);
		when(UserTypeChecker.isGuestUser(userTokenInfo)).thenReturn(false);
		ResponseEntity entity = messageResource.getConversationMessages(anyInt(), userTokenInfo);
		Assert.assertEquals(500, entity.getStatusCodeValue());

	}

	@SuppressWarnings("rawtypes")
	@Test
	public void testgetConversationMessages2() {
		PowerMockito.mockStatic(UserTypeChecker.class);
		UserTokenInfo userTokenInfo = ObjectBuilder.getUserTokenInfo();
		@SuppressWarnings("unused")
		Message[] msg = { new Message(), new Message() };
		when(messageService.getConversationMessages(anyInt())).thenReturn(null);
		when(UserTypeChecker.isGuestUser(userTokenInfo)).thenReturn(false);
		ResponseEntity entity = messageResource.getConversationMessages(anyInt(), userTokenInfo);
		Assert.assertEquals(500, entity.getStatusCodeValue());

	}

	@SuppressWarnings("rawtypes")
	@Test
	public void testgetLinkToProceed() {
		UserTokenInfo userTokenInfo = ObjectBuilder.getUserTokenInfo();
		when(messageService.initialClickToSendMessage(any())).thenReturn(true);
		ResponseEntity entity = messageResource.getLinkToProceed(userTokenInfo);
		Assert.assertEquals(200, entity.getStatusCodeValue());
	}

	@SuppressWarnings("rawtypes")
	@Test
	public void testgetLinkToProceed1() {
		UserTokenInfo userTokenInfo = ObjectBuilder.getUserTokenInfo();
		when(messageService.initialClickToSendMessage(any())).thenReturn(false);
		ResponseEntity entity = messageResource.getLinkToProceed(userTokenInfo);
		Assert.assertEquals(403, entity.getStatusCodeValue());
	}

	@SuppressWarnings("rawtypes")
	@Test
	public void testdeleteMessage() {
		PowerMockito.mockStatic(UserTypeChecker.class);
		UserTokenInfo userTokenInfo = ObjectBuilder.getUserTokenInfo();
		when(UserTypeChecker.isGuestUser(any())).thenReturn(false);
		when(messageService.deleteMessage(anyInt())).thenReturn(true);
		ResponseEntity entity = messageResource.deleteMessage(anyInt(), userTokenInfo);
		Assert.assertEquals(200, entity.getStatusCodeValue());
	}

	@SuppressWarnings("rawtypes")
	@Test
	public void testdeleteMessage1() {
		PowerMockito.mockStatic(UserTypeChecker.class);
		UserTokenInfo userTokenInfo = ObjectBuilder.getUserTokenInfo();
		when(UserTypeChecker.isGuestUser(any())).thenReturn(false);
		when(messageService.deleteMessage(anyInt())).thenReturn(false);
		ResponseEntity entity = messageResource.deleteMessage(anyInt(), userTokenInfo);
		Assert.assertEquals(500, entity.getStatusCodeValue());
	}

	@SuppressWarnings("rawtypes")
	@Test
	public void testdeleteMessage2() {
		PowerMockito.mockStatic(UserTypeChecker.class);
		UserTokenInfo userTokenInfo = ObjectBuilder.getUserTokenInfo();
		when(UserTypeChecker.isGuestUser(any())).thenReturn(true);
		when(messageService.deleteMessage(anyInt())).thenReturn(false);
		ResponseEntity entity = messageResource.deleteMessage(anyInt(), userTokenInfo);
		Assert.assertEquals(403, entity.getStatusCodeValue());
	}

	@SuppressWarnings("rawtypes")
	@Test
	public void testdeleteUserConversation() {
		PowerMockito.mockStatic(UserTypeChecker.class);
		UserTokenInfo userTokenInfo = ObjectBuilder.getUserTokenInfo();
		when(UserTypeChecker.isGuestUser(any())).thenReturn(false);
		ResponseEntity entity = messageResource.deleteUserConversation(2, 1, userTokenInfo);
		Assert.assertEquals(200, entity.getStatusCodeValue());

	}

	@SuppressWarnings("rawtypes")
	@Test
	public void testdeleteUserConversation1() {
		PowerMockito.mockStatic(UserTypeChecker.class);
		UserTokenInfo userTokenInfo = ObjectBuilder.getUserTokenInfo();
		when(UserTypeChecker.isGuestUser(any())).thenReturn(true);
		ResponseEntity entity = messageResource.deleteUserConversation(1, 2, userTokenInfo);
		Assert.assertEquals(403, entity.getStatusCodeValue());

	}
}
