package com.wavelabs.repo.tests;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.google.gson.Gson;
import com.wavelabs.dao.UserDao;
import com.wavelabs.entity.User;
import com.wavelabs.model.test.ObjectBuilder;
import com.wavelabs.repos.UserRepo;
import com.wavelabs.services.UserTypeChecker;

@RunWith(PowerMockRunner.class)
@PrepareForTest(UserTypeChecker.class)
public class UserDaoTest {
	@Mock
	UserRepo userRepo;
	@InjectMocks
	UserDao userDao;

	@Test
	public void testGetUser() {
		User user = ObjectBuilder.getUser();
		when(userRepo.findById(anyLong())).thenReturn(user);
		User u = userDao.getUser(3);
		Gson gson = new Gson();
		Assert.assertEquals(gson.toJson(user), gson.toJson(u));
	}

	@Test
	public void testPersistUser() {

		User user = ObjectBuilder.getUser();
		when(userRepo.save(any(User.class))).thenReturn(user);
		boolean flag = userDao.saveUser(user);
		Assert.assertEquals(true, flag);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testPersistUserIsFalse() {

		User user = ObjectBuilder.getUser();
		when(userRepo.save(any(User.class))).thenThrow(Exception.class);
		boolean flag = userDao.saveUser(user);
		Assert.assertEquals(false, flag);

	}

	@Test
	public void testDeleteUser() {
		User user = ObjectBuilder.getUser();
		//when(userRepo.delete(anyInt())).thenThrow(Exception.class);
		boolean flag = userDao.saveUser(user);
		Assert.assertEquals(true, flag);
	}
}
