package com.wavelabs.model.test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.wavelabs.models.enums.Status;

public class StatusTest {
	
	@Test
	public void testStatus(){
		assertThat(Status.valueOf("ACCEPT"), is(notNullValue()));
		assertThat(Status.valueOf("DECLINE"), is(notNullValue()));
		assertThat(Status.valueOf("VIEWED"), is(notNullValue()));
	}

}
