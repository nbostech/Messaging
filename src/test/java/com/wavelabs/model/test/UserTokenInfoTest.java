package com.wavelabs.model.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.wavelabs.entity.UserTokenInfo;

@RunWith(MockitoJUnitRunner.class)
public class UserTokenInfoTest {
	@InjectMocks
	UserTokenInfo userTokenInfoMock;

	UserTokenInfo userTokenInfo;

	@Before
	public void setUp() {
		userTokenInfo = ObjectBuilder.getUserTokenInfo();
	}

	/*@Test
	public void testParameterizedConstructor() {
		userTokenInfoMock.setId(userTokenInfo.getId());
		Assert.assertEquals(userTokenInfo.getId(), userTokenInfoMock.getId());
		userTokenInfoMock.setEmail(userTokenInfo.getEmail());
		Assert.assertEquals(userTokenInfo.getEmail(), userTokenInfoMock.getEmail());
		userTokenInfoMock.setPhone(userTokenInfo.getPhone());
		Assert.assertEquals(userTokenInfo.getPhone(), userTokenInfoMock.getPhone());
		userTokenInfoMock.setUsername(userTokenInfo.getUsername());
		Assert.assertEquals(userTokenInfo.getUsername(), userTokenInfoMock.getUsername());
		userTokenInfoMock.setClientId(userTokenInfo.getClientId());
		Assert.assertEquals(userTokenInfo.getClientId(), userTokenInfoMock.getClientId());
		userTokenInfoMock.setTokenType(userTokenInfo.getTokenType());
		Assert.assertEquals(userTokenInfo.getTokenType(), userTokenInfoMock.getTokenType());
		userTokenInfoMock.setExpiration(userTokenInfo.getExpiration());
		Assert.assertEquals(userTokenInfo.getExpiration(), userTokenInfoMock.getExpiration());
		userTokenInfoMock.setMember(userTokenInfo.getMember());
		Assert.assertEquals(userTokenInfo.getMember().getUuid(), userTokenInfoMock.getMember().getUuid());
		userTokenInfoMock.setModules(userTokenInfo.getModules());
		Assert.assertEquals(userTokenInfo.getModules().length, userTokenInfoMock.getModules().length);
	}*/

	@Test
	public void testId() {

		userTokenInfoMock.setId(userTokenInfo.getId());
		Assert.assertEquals(userTokenInfo.getId(), userTokenInfoMock.getId());

	}

	@Test
	public void testEmail() {
		userTokenInfoMock.setEmail(userTokenInfo.getEmail());
		Assert.assertEquals(userTokenInfo.getEmail(), userTokenInfoMock.getEmail());

	}

	@Test
	public void testPhone() {
		userTokenInfoMock.setPhone(userTokenInfo.getPhone());
		Assert.assertEquals(userTokenInfo.getPhone(), userTokenInfoMock.getPhone());
	}

	@Test
	public void testUserName() {
		userTokenInfoMock.setUsername(userTokenInfo.getUsername());
		Assert.assertEquals(userTokenInfo.getUsername(), userTokenInfoMock.getUsername());
	}

	@Test
	public void testClientId() {
		userTokenInfoMock.setClientId(userTokenInfo.getClientId());
		Assert.assertEquals(userTokenInfo.getClientId(), userTokenInfoMock.getClientId());
	}

	@Test
	public void testTokenType() {
		userTokenInfoMock.setTokenType(userTokenInfo.getTokenType());
		Assert.assertEquals(userTokenInfo.getTokenType(), userTokenInfoMock.getTokenType());
	}

	@Test
	public void testToken() {
		userTokenInfoMock.setToken(userTokenInfo.getToken());
		Assert.assertEquals(userTokenInfo.getToken(), userTokenInfoMock.getToken());
	}

	@Test
	public void testExpiration() {
		userTokenInfoMock.setExpiration(userTokenInfo.getExpiration());
		Assert.assertEquals(userTokenInfo.getExpiration(), userTokenInfoMock.getExpiration());
	}

	@Test
	public void testExpired() {
		userTokenInfoMock.setExpired(userTokenInfo.isExpired());
		Assert.assertEquals(userTokenInfo.isExpired(), userTokenInfoMock.isExpired());
	}

	@Test
	public void testTenantId() {
		userTokenInfoMock.setTenantId(userTokenInfo.getTenantId());
		Assert.assertEquals(userTokenInfo.getTenantId(), userTokenInfoMock.getTenantId());
	}

	@Test
	public void testMember() {
		userTokenInfoMock.setMember(userTokenInfo.getMember());
		Assert.assertEquals(userTokenInfo.getMember().getUuid(), userTokenInfoMock.getMember().getUuid());
	}

	@Test
	public void testModules() {
		userTokenInfoMock.setModules(userTokenInfo.getModules());
		Assert.assertEquals(userTokenInfo.getModules().length, userTokenInfoMock.getModules().length);
	}

}
