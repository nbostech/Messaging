package com.wavelabs.model.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.wavelabs.entity.User;
@RunWith(MockitoJUnitRunner.class)
public class UserTest {
	/*
	 * @Test public void testSetFirstName() throws NoSuchFieldException,
	 * SecurityException, IllegalArgumentException, IllegalAccessException{
	 * final User user = new User(); //when user.setFirstName("AAA"); //then
	 * final Field field = user.getClass().getDeclaredField("AAA");
	 * field.setAccessible(true); assertEquals("Fields didn't match",
	 * field.get(user),"AAA"); assertEquals(true,
	 * user.getFirstName().equals(user)); }
	 */
	@Test
	public void testDefaultConstructor() {
		User user = new User();
		user.setTenantId("a123a");
		assertEquals("not match", "a123a", user.getTenantId());
		user.setUuid("a234");
		assertEquals("not match", "a234", user.getUuid());
		user.setUserName("Thejasree");
		assertEquals("Thejasree", user.getUserName());

	}

	@Test
	public void testSetUserName() {
		User user = new User();
		user.setUserName("aaa");
		assertEquals("Didn't match", user.getUserName() == "aaa", true);
	}

	@Test
	public void testSetId() {
		User user = new User();
		user.setId(2l);
		assertEquals("didn't match", user.getId() == 2l, true);
	}

	@Test
	public void testTenantId() {
		User user = new User();
		user.setTenantId("aaa123");
		assertEquals("not given", user.getTenantId(), "aaa123");
	}

	@Test
	public void testUuid() {
		User user = new User();
		user.setUuid("123abc");
		assertEquals("not given", user.getUuid(), "123abc");
	}

}
