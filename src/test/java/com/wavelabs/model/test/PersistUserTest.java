package com.wavelabs.model.test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wavelabs.entity.User;
import com.wavelabs.services.PersistUser;
import com.wavelabs.services.UserService;
@RunWith(MockitoJUnitRunner.class)
public class PersistUserTest {
	@Mock
	UserService userService;
	@InjectMocks
	PersistUser persist;

	@Test
	public void testPersistUser() {
		when(userService.saveUser(any(User.class))).thenReturn(true);
		Boolean flag = persist.persistUser(mock(User.class));
		assertEquals(true, flag);

	}
}
