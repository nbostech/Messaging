package com.wavelabs.model.test;


import static org.junit.Assert.assertEquals;

import java.sql.Timestamp;

import org.junit.Test;

import com.wavelabs.entity.Message;
import com.wavelabs.entity.MessageThread;
import com.wavelabs.entity.User;


public class MessageTest {
	
	Message msg = new Message();
	User user = new User();
	MessageThread mt = new MessageThread();
	@Test
	public void testDefaultConsturctor(){
	    msg.setFromId(user);
	    assertEquals(user,msg.getFromId());
	    msg.setToId(user);
	    assertEquals(user,msg.getToId());
	    msg.setBody("Hi");
	    assertEquals("Hi",msg.getBody());
	    msg.setSubject("Hey");
	    assertEquals("Hey",msg.getSubject());
	    msg.setMessagethread(mt);
	    assertEquals(mt,msg.getMessagethread());
	    msg.setIs_reply(false);
	    assertEquals(false, false);
	}
	
	@Test
	public void testId() {
		msg.setId(2);
		assertEquals("Didn't match",msg.getId() == 2, true);
	}
	@Test
	public void testBody(){
		msg.setBody("Hello");
		assertEquals("didn't match", msg.getBody() == "Hello", true);
		
	}
	@Test
	public void testSubject(){
		msg.setSubject("Hi");
		assertEquals("didn't match", msg.getSubject() == "Hi", true);
		
	}
	@Test
	public void testTenantId(){
		msg.setTenantId("ab12");
		assertEquals("didn't match", msg.getTenantId() == "ab12", true);
		
	}
	@Test
	public void testFromId(){
		msg.setFromId(user);
		assertEquals("didn't match", msg.getFromId() == user, true);
		
	}
	@Test
	public void testToId(){
		msg.setToId(user);;
		assertEquals("didn't match", msg.getToId() == user, true);
		
	}
	@Test
	public void testMessageThread(){
		msg.setMessagethread(mt);
		assertEquals("didn't match", msg.getMessagethread() == mt, true);
		
	}
		
	@Test
	public void testTimeStamp(){
		
		Timestamp timestamp = Timestamp.valueOf("2007-12-23 09:01:06.000000003");
		 assertEquals("not given", timestamp.equals("2007-12-23 09:01:06.000000003"), false);
	}
	@Test
	public void testIsreply(){
		msg.setIs_reply(false);;
		assertEquals("not given", msg.isIs_reply() == false, true);
		
	}
}
