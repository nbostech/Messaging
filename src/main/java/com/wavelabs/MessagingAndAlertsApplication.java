package com.wavelabs;

import javax.servlet.Filter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.web.DispatcherServletAutoConfiguration;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.DispatcherServlet;

import com.wavelabs.entity.Message;
import com.wavelabs.entity.MessageThread;
import com.wavelabs.services.MessageService;
import com.wavelabs.services.PersistUser;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@SpringBootApplication
@EnableJpaRepositories("com.wavelabs.repos")
@ComponentScan({ "com.wavelabs" })
@EntityScan("com.wavelabs.entity")
public class MessagingAndAlertsApplication {
	/**
	 * Method that Spring Boot detects when you run the executable jar file.
	 * 
	 * @param args
	 * @throws Exception
	 */

	public static void main(String[] args) {
		SpringApplication.run(MessagingAndAlertsApplication.class, args);

	}

	/**
	 * Enables Spring's annotation-driven cache management capability, similar
	 * to the support found in Spring's <cache:*> XML namespace.
	 * 
	 * @return GuavaCacheManager
	 */
	/*
	 * @Bean public CacheManager cacheManager() { GuavaCacheManager cacheManager
	 * = new GuavaCacheManager("greetings"); return cacheManager; }
	 */

	/**
	 * 
	 * {@link ServletRegistrationBean servletRegistrationBean} is class with a
	 * spring bean friendly design that is used to register servlets in a
	 * servlet 3.0 container within spring boot. Register dispatcherServlet
	 * programmatically
	 * 
	 * @return ServletRegistrationBean
	 */
	@Bean
	public ServletRegistrationBean dispatcherServletRegistration() {
		ServletRegistrationBean registration = new ServletRegistrationBean(dispatcherServlet(), "/*");
		registration.setName(DispatcherServletAutoConfiguration.DEFAULT_DISPATCHER_SERVLET_REGISTRATION_BEAN_NAME);
		return registration;
	}

	/**
	 * To add your own servlet and map it to the root resource. Default would be
	 * root of your application(/)
	 * 
	 * @return DispatcherServlet
	 */
	@Bean
	public DispatcherServlet dispatcherServlet() {
		return new DispatcherServlet();
	}

	/**
	 * Method to set the order for the filters
	 * 
	 * @return
	 */
	@Bean
	public Filter springFilter() {
		return new IDNFilter();
	}

	@Bean
	public AuthenticationTokenCache authenticationTokenCache() {

		return new AuthenticationTokenCache();

	}

	@Bean
	public PersistUser persistUser() {
		return new PersistUser();
	}

	@Bean
	public MessageService messageService() {
		return new MessageService();
	}

	@Bean
	public Message message() {
		return new Message();
	}

	@Bean
	public MessageThread messageThread() {
		return new MessageThread();
	}

}
