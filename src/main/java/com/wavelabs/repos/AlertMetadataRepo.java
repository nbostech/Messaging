package com.wavelabs.repos;

import org.springframework.data.repository.CrudRepository;

import com.wavelabs.entity.AlertMetadata;

public interface AlertMetadataRepo extends CrudRepository<AlertMetadata, Integer>{

}
