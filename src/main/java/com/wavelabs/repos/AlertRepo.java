package com.wavelabs.repos;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.wavelabs.entity.Alert;
import com.wavelabs.entity.User;

public interface AlertRepo extends CrudRepository<Alert, Integer> {

	Alert findAlertById(int id);

	Alert findAndUpdateAlertById(Alert alert);

	@Query("select al from Alert al where al.toId =:id")
	List<Alert> findAllAlertsById(@Param("id") User toId);


}
