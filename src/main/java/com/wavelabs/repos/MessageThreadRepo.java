package com.wavelabs.repos;

import org.springframework.data.repository.CrudRepository;

import com.wavelabs.entity.MessageThread;

public interface MessageThreadRepo extends CrudRepository<MessageThread, Integer> {
	
 MessageThread findById(int id);
 

}
