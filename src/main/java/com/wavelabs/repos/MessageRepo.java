package com.wavelabs.repos;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.wavelabs.entity.Message;
import com.wavelabs.entity.MessageThread;
import com.wavelabs.entity.User;

public interface MessageRepo extends CrudRepository<Message, Integer> {

	
	@Query("select m from Message m where m.messagethread=:id")
	List<Message> findAllById(@Param("id") int id);

	//Message[] findById(int id);

	Boolean delete(int id);

	@Query("select distinct messagethread from Message m where m.toId=:toId")
	List<MessageThread> findByToId(@Param("toId") User toId);
	@Query("Select m from Message m where timeStamp = (select max(timeStamp) from Message m where messagethread=:mt)")
	Message findByMessagethread(@Param("mt") MessageThread mt);
	
	@Query("select m from Message m where messagethread=:mt")
	Message[] findAllMessagesOfGivenThread(@Param("mt") MessageThread mt);
	
	Boolean deleteById(@Param("user_id") int userId, @Param("message_thread_id") int message_thread_id);
	
	//Load the messageThreads 
	
	//Load the latest messages of messageThread
	
	
}
