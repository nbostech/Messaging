package com.wavelabs.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wavelabs.entity.Alert;
import com.wavelabs.entity.UserTokenInfo;
import com.wavelabs.services.AlertService;
import com.wavelabs.services.UserTypeChecker;

import io.nbos.capi.api.v0.models.RestMessage;

@RestController
@Component
@RequestMapping(value = "api/messagingandalert/v1/alert")
public class AlertController {
	@Autowired
	private AlertService alertService;
	RestMessage rm = new RestMessage();

	@SuppressWarnings("rawtypes")
	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)

	public ResponseEntity saveAlert(@RequestBody Alert alert,
			@RequestAttribute("userTokenInfo") UserTokenInfo userTokenInfo) {
		Boolean flag1 = UserTypeChecker.isGuestUser(userTokenInfo);
		if (!flag1) {
			Boolean flag = alertService.saveAlert(alert);
			if (!flag) {
				rm.message = "Alert is not created!";
				rm.messageCode = "500";
				return ResponseEntity.status(500).body(rm);
			}
			rm.message = "Alert is created successfully!!";
			rm.messageCode = "200";
			return ResponseEntity.status(200).body(rm);

		} else {
			rm.message = "Please login into App.!!";
			rm.messageCode = "403";
			return ResponseEntity.status(403).body(rm);

		}

	}

	@SuppressWarnings("rawtypes")
	@RequestMapping("/{id}")
	public ResponseEntity getAlert(@PathVariable("id") Integer id,
			@RequestAttribute("userTokenInfo") UserTokenInfo userTokenInfo) {
		Boolean flag = UserTypeChecker.isGuestUser(userTokenInfo);
		if (!flag) {
			Alert alert = alertService.getAlert(id);
			if (alert == null) {
				rm.message = "failed!!";
				rm.messageCode = "500";
				return ResponseEntity.status(500).body(rm);
			}
			rm.messageCode = "200";
			return ResponseEntity.status(200).body(alert);
		}
		rm.message = "Please login into App.!!";
		rm.messageCode = "403";
		return ResponseEntity.status(403).body(rm);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/received/{id}")

	public ResponseEntity getAllAlertsPerReceiver(@PathVariable("id") int id,
			@RequestAttribute("userTokenInfo") UserTokenInfo userTokenInfo) {
		Boolean flag = UserTypeChecker.isGuestUser(userTokenInfo);
		if (!flag) {
			List<Alert> alert = alertService.getAllAlertsPerReceiver(id);
			if (alert.isEmpty()) {
				rm.message = "Alert retrieval failed!!!";
				rm.messageCode = "500";
				return ResponseEntity.status(500).body(rm);
			}
			return ResponseEntity.status(200).body(alert);
		} else {
			rm.message = "Please login into app.!";
			rm.messageCode = "403";
			return ResponseEntity.status(403).body(rm);
		}
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
	public ResponseEntity getAlertAndUpdate(@PathVariable("id") int id, @RequestBody Alert alert,
			@RequestAttribute("userTokenInfo") UserTokenInfo userTokenInfo) {
		Boolean flag = UserTypeChecker.isGuestUser(userTokenInfo);
		if (!flag) {
			alert.setId(id);
			Alert alert1 = alertService.updateAlert(alert);

			if (alert1 == null) {
				rm.message = "Alert updation failed!!";
				rm.messageCode = "500";
				return ResponseEntity.status(500).body(rm);
			}
			rm.message = "Alert is updated!.!";
			rm.messageCode = "200";
			return ResponseEntity.status(200).body(rm);
		}
		rm.message = "Please login into app.!";
		rm.messageCode = "403";
		return ResponseEntity.status(403).body(rm);
	}
}
