package com.wavelabs.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wavelabs.entity.User;
import com.wavelabs.entity.UserTokenInfo;
import com.wavelabs.services.UserService;
import com.wavelabs.services.UserTypeChecker;

import io.nbos.capi.api.v0.models.RestMessage;

@RestController
@Component
@RequestMapping(value = "api/messagingandalert/v1/user")
public class UserController {
	@Autowired
	private UserService userService;
	RestMessage rm = new RestMessage();

	private UserController(UserService userService) {
		this.userService = userService;
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(method = org.springframework.web.bind.annotation.RequestMethod.POST)
	public ResponseEntity createUser(@RequestBody User user,
			@RequestAttribute("userTokenInfo") UserTokenInfo userTokenInfo) {

		Boolean flag1 = UserTypeChecker.isGuestUser(userTokenInfo);
		if (flag1) {
			Boolean flag = userService.saveUser(user);
			if (!flag) {
				rm.message = "User is not created!";
				rm.messageCode = "500";
				return ResponseEntity.status(500).body(rm);
			}
			rm.message = "User is created!";
			rm.messageCode = "200";
			return ResponseEntity.status(200).body(rm);
		} else {
			rm.message = "Please login into app...!";
			rm.messageCode = "403";
			return ResponseEntity.status(403).body(rm);
		}
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping("/{id}")
	public ResponseEntity getUser(@PathVariable("id") Integer id) {
		User user = userService.getUser(id);
		if (user == null) {
			rm.message = "Failed, try again!!!";
			rm.messageCode = "500";
			return ResponseEntity.status(500).body(rm);

		} else {
			rm.message = "Sucess!!!";
			rm.messageCode = "200";
			return ResponseEntity.status(200).body(user);
		}
	}

	/*@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/{id}", method = org.springframework.web.bind.annotation.RequestMethod.DELETE)
	public ResponseEntity deleteUser(@PathVariable("id") Integer id,
			@RequestAttribute("userTokenInfo") UserTokenInfo userTokenInfo) {
		Boolean flag1 = UserTypeChecker.isGuestUser(userTokenInfo);
		if (!flag1) {
			Boolean flag = userService.deleteUser(id);
			if (!flag) {
				rm.message = "Failed, try again!!!";
				rm.messageCode = "500";
				return ResponseEntity.status(500).body(rm);
			}
			rm.message = "User deleted successfully!!!";
			rm.messageCode = "200";
			return ResponseEntity.status(200).body(rm);
		} else {
			rm.message = "Please login into app.!!";
			rm.messageCode = "403";
			return ResponseEntity.status(403).body(rm);
		}

	}
*/
}
