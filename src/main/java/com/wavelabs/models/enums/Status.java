package com.wavelabs.models.enums;

public enum Status {
	ACCEPT, DECLINE, VIEWED;
}
