package com.wavelabs.models.enums;

public enum AlertType {
 ACCEPTDECLINE,
 VIEWONLY,
 VIEWDESTINATION;
}
