package com.wavelabs.services;


import java.util.Collections;
import java.util.List;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wavelabs.entity.Alert;
import com.wavelabs.entity.User;
import com.wavelabs.repos.AlertRepo;
import com.wavelabs.repos.UserRepo;

@Service
public class AlertService {
	@Autowired
	private AlertRepo alertRepo;
	@Autowired
	UserRepo userRepo;
	static Logger logger = Logger.getLogger(AlertService.class);

	public  Boolean saveAlert(Alert alert) {
		try {
			alertRepo.save(alert);
			return true;
		} catch (Exception e) {
			logger.info(e);
			return false;
		} 
	}
	

	public  Alert getAlert(int id) {
		try{
			return  alertRepo.findAlertById(id);
	
			
		} catch (Exception e) {
			logger.info(e);
			return null;
		}
	}

	public  List<Alert> getAllAlertsPerReceiver(long id) {
		try{
			User toId = userRepo.findById(id);
			return alertRepo.findAllAlertsById(toId);
			
			
			}catch(Exception e){
				logger.info(e);
				return Collections.emptyList();
			
			}
	}

	public  Alert updateAlert(Alert alert) {
		try{
			alertRepo.save(alert);
			return alert;
		} catch (Exception e) {
			logger.info(e);
			return null;
		} 
	}
}

