package com.wavelabs.services;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wavelabs.entity.UserTokenInfo;
import com.wavelabs.repos.UserRepo;
@Component
public class UserTypeChecker {

	private static final Logger logger = Logger.getLogger(UserTypeChecker.class);
@Autowired
UserRepo userRepo;
	private UserTypeChecker() {
	}

	public static boolean isGuestUser(UserTokenInfo userTokenInfo) {
		return (userTokenInfo.getUsername() == null);
	}
	public static boolean isUserExists(String tenantId, String uuid, String userName) {
		return true;

	}

}
