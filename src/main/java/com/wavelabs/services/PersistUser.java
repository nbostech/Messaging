package com.wavelabs.services;

import org.springframework.beans.factory.annotation.Autowired;

import com.wavelabs.entity.User;

public class PersistUser {
	@Autowired
	UserService userService;
	public PersistUser(){
	}
	public Boolean persistUser(User user){
		return userService.saveUser(user);
	}

}
