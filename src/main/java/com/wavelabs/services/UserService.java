package com.wavelabs.services;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wavelabs.entity.User;
import com.wavelabs.repos.UserRepo;

@Service
public class UserService {
	@Autowired
	UserRepo userRepo;
	static Logger logger = Logger.getLogger(UserService.class);

	public Boolean saveUser(User user) {
		try {
			userRepo.save(user);
			return true;
		} catch (Exception e) {
			logger.info(e);
			return false;
		}
	}

	public User getUser(long id) {
		return userRepo.findById(id);
	}

	public Boolean deleteUser(int id) {
		try {
			userRepo.delete(id);
			return true;
		} catch (Exception e) {
			logger.info(e);
			return false;
		}

	}

}
