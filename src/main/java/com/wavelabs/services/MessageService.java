package com.wavelabs.services;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wavelabs.entity.Message;
import com.wavelabs.entity.MessageThread;
import com.wavelabs.entity.User;
import com.wavelabs.entity.UserTokenInfo;
import com.wavelabs.repos.MessageRepo;
import com.wavelabs.repos.MessageThreadRepo;
import com.wavelabs.repos.UserRepo;

@Service
public class MessageService {
	@Autowired
	MessageRepo messageRepo;
	@Autowired
	MessageThreadRepo messageThreadRepo;
	@Autowired
	UserRepo userRepo;
	@Autowired
	EntityManager entityManager;
	@Autowired
	Message message;
	static Logger logger = Logger.getLogger(MessageService.class);

	public MessageService() {

	}

	public Message[] getLatestMessageFromAllConversations(long id) {

		User toId = userRepo.findById(id);
		List<MessageThread> mts = messageRepo.findByToId(toId);
		Message[] messages = new Message[mts.size()];
		int j = 0;
		for (MessageThread mt : mts) {
			messages[j++] = messageRepo.findByMessagethread(mt);
		}
		return messages;

	}

	public Message[] getConversationMessages(int id) {
		try {
			MessageThread thread = messageThreadRepo.findOne(id);
			return messageRepo.findAllMessagesOfGivenThread(thread);
		
		} catch (Exception e) {
			logger.info(e);
			return new Message[]{};
		}

	}


	public Boolean saveMessage(Message message) {
		try {
			MessageThread mt = message.getMessagethread();
			message.setTimeStamp(Calendar.getInstance());
			if (mt == null) {
				mt = new MessageThread();
				Set<User> users = new HashSet<>();
				User from = message.getFromId();
				User to = message.getToId();
				users.add(to);
				users.add(from);
				mt.setUser(users);
				messageThreadRepo.save(mt);
				message.setMessagethread(mt);

			} else {
				MessageThread replyMessage = messageThreadRepo.findById(message.getMessagethread().getId());
				message.setMessagethread(replyMessage);
			}
			messageRepo.save(message);
			return true;
		} catch (Exception e) {
			logger.info(e);
			return false;
		}
	}

	public Boolean deleteUserConversation(long userId, int message_thread_id) {
		try {
			MessageThread thread = messageThreadRepo.findById(message_thread_id);
			Set<User> users = thread.getUser();
			for (User user : users) {
				if (user.getId() == userId) {
					users.remove(user);
					break;
				}
			}
			thread.setUser(users);
			messageThreadRepo.save(thread);
			return true;
		} catch (Exception e) {
			logger.info(e);
			return false;
		}

	}

	public Boolean deleteMessage(int id) {
		try {
			messageRepo.delete(id);
			return true;
		} catch (Exception e) {
			logger.info(e);
			return false;
		}
	}

	public Boolean initialClickToSendMessage(UserTokenInfo userTokenInfo) {
		User user = new User();
		try {
			user.setTenantId(userTokenInfo.getTenantId());
			user.setUserName(userTokenInfo.getUsername());
			user.setUuid(userTokenInfo.getMember().getUuid());
			userRepo.save(user);
			return true;
		} catch (Exception e) {
			logger.info(e);
			return false;

		}

	}

}
