package com.wavelabs.entity;

import java.io.Serializable;
import java.util.Calendar;

public class UserTokenInfo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private String email;
	private String phone;
	private String username;
	private String clientId;
	private String tokenType;
	private String token;
	private Calendar expiration;
	private boolean expired;
	private String tenantId;
	private Modules[] modules;
	private Member member;

	public UserTokenInfo() {
	}

	
	public UserTokenInfo(Long id, String email, String phone, String username, String clientId, String tokenType,
			String token, Calendar expiration, boolean expired, String tenantId, Modules[] modules, Member member) {
		super();
		this.id = id;
		this.email = email;
		this.phone = phone;
		this.username = username;
		this.clientId = clientId;
		this.tokenType = tokenType;
		this.token = token;
		this.expiration = expiration;
		this.expired = expired;
		this.tenantId = tenantId;
		this.modules = modules;
		this.member = member;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getTokenType() {
		return tokenType;
	}

	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Calendar getExpiration() {
		return expiration;
	}

	public void setExpiration(Calendar expiration) {
		this.expiration = expiration;
	}

	public boolean isExpired() {
		return expired;
	}

	public void setExpired(boolean expired) {
		this.expired = expired;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public Modules[] getModules() {
		return modules;
	}

	public void setModules(Modules[] modules) {
		this.modules = modules;
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

}
