package com.wavelabs.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.wavelabs.models.enums.Status;
@Entity
@Table(name="Alert")
public class Alert {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private int id;
	
	@ManyToOne(fetch =FetchType.EAGER,  cascade = {CascadeType.MERGE,CascadeType.REFRESH}, targetEntity= User.class)
	@JoinColumn(name = "fromId")
	private User fromId;
	@ManyToOne(fetch =FetchType.EAGER,  cascade = {CascadeType.MERGE,CascadeType.REFRESH}, targetEntity= User.class)
	@JoinColumn(name = "toId")
	private User toId;
	
	@Column(name="text")
	private String text;
	
	@ManyToOne(fetch =FetchType.EAGER, cascade = {CascadeType.MERGE,CascadeType.REFRESH})
	@JoinColumn(name = "metadata_id")
	private AlertMetadata metadata;
	
	@Column(name="status")
	@Enumerated(EnumType.STRING)
	private Status status;
	@Column(name="tenantId")
	private String tenantId;

	public Alert() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getFromId() {
		return fromId;
	}

	public void setFromId(User fromId) {
		this.fromId = fromId;
	}

	public User getToId() {
		return toId;
	}

	public void setToId(User toId) {
		this.toId = toId;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public AlertMetadata getMetadata() {
		return metadata;
	}

	public void setMetadata(AlertMetadata metadata) {
		this.metadata = metadata;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

}
