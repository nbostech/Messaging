package com.wavelabs.entity;

import java.util.Calendar;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 
 * @author thejasreem
 *
 */
@Entity
@Table(name="Message")
public class Message {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;
	@Column(name = "subject")
	private String subject;
	@Column(name = "body")
	private String body;
	@Column(name = "isreply")
	private boolean is_reply;
	
	@ManyToOne(fetch =FetchType.EAGER,  cascade = {CascadeType.MERGE,CascadeType.REFRESH}, targetEntity= User.class)
	@JoinColumn(name = "fromId")
	private User fromId;
	
	@ManyToOne(fetch =FetchType.EAGER,cascade ={CascadeType.MERGE,CascadeType.REFRESH}, targetEntity=User.class)
	@JoinColumn(name = "toId")
	private User toId;
	@ManyToOne(fetch =FetchType.EAGER,cascade = {CascadeType.MERGE,CascadeType.REFRESH})
	@JoinColumn(name = "message_thread_id")
	private MessageThread messagethread;

	@Column(name = "timeStamp")
	private Calendar timeStamp;
	@Column(name = "tenantId")
	private String tenantId;

	public Message() {

	}

	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public boolean isIs_reply() {
		return is_reply;
	}

	public void setIs_reply(boolean is_reply) {
		this.is_reply = is_reply;
	}

	public User getFromId() {
		return fromId;
	}

	public void setFromId(User fromId) {
		this.fromId = fromId;
	}

	public User getToId() {
		return toId;
	}

	public void setToId(User toId) {
		this.toId = toId;
	}

	public MessageThread getMessagethread() {
		return messagethread;
	}

	public void setMessagethread(MessageThread messagethread) {
		this.messagethread = messagethread;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public Calendar getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(Calendar timeStamp) {
		this.timeStamp = timeStamp;
	}

}
