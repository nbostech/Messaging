package com.wavelabs.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.wavelabs.models.enums.AlertType;

@Entity
@Table(name = "AlertMetadata")
public class AlertMetadata {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	private int id;
	@Column(name = "alertType")
	@Enumerated(EnumType.STRING)
	private AlertType alertType;

	public AlertMetadata() {

	}

	public AlertMetadata(int id, AlertType alertType) {
		super();
		this.id = id;
		this.alertType = alertType;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public AlertType getAlertType() {
		return alertType;
	}

	public void setAlertType(AlertType alertType) {
		this.alertType = alertType;
	}

}
